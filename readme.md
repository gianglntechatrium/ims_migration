## How to run the migration:
# Step 1: DB setting
- Edit database.php to update the mysql setting:
+ mysql: final database after migration (find data schema under IMS 2.0 project)
+ senims: current IMS production (old IMS System)
+ ims: location data (find under IMS 2.0 project)

# Step 2: Run migration 
php artisan db:seed
