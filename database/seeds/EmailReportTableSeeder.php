<?php

use Illuminate\Database\Seeder;

class EmailReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emailidx = 0;
        $total = DB::connection('senims')->select("select count(id) total from email_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06')))")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $email_messages = DB::connection('senims')->select("select message_id, email_send_to, email_subject, email_body,
                                                                  email_status, created_at, updated_at, is_archived
                                                            from email_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06'))) order by id desc limit 1000 offset ".($i * 1000));
            foreach ($email_messages as $m) {
                $updated_at = "";
                if (!empty($m->updated_at)) {
                    $myTime = strtotime($m->updated_at);
                    $updated_at = date('Y-m-d H:i:s', $myTime);
                }
                $resultEmail = DB::connection('mysql')->table('report')->where('id', $m->message_id)
                    ->update([
                        'email' => $m->email_send_to,
                        'email_subject' => $m->email_subject,
                        'email_content' => $m->email_body,
                        'email_status' => $m->email_status == 'Sent' ? 4 : 3,
                        'is_archived' => $m->is_archived,
                        'updated_at' => $m->updated_at
                    ]);
                print_r("\n Email Records: " . $emailidx++);
                print_r(" ==================> inserted: " . $resultEmail);
            }
        }
    }
}
