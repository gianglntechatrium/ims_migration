<?php

use Illuminate\Database\Seeder;

class UpdateSMSMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sms_messages
        $smsidx = 0;
        $total = DB::connection('senims')->select("select count(id) total from sms_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06')))")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $email_messages = DB::connection('senims')->select("select message_id, sms_send_to, sms_message, sms_status,
                                                                  sms_respond_code, updated_at
                                                            from sms_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06'))) order by id desc limit 1000 offset ".($i * 1000));
            foreach ($email_messages as $m) {
                $updated_at = "";
                if (!empty($m->updated_at)) {
                    $myTime = strtotime($m->updated_at);
                    date_default_timezone_set("UTC");
                }
                $status = 1;
                switch ($m->sms_status) {
                    case 'Processing':
                        $status = 1;
                        break;
                    case 'Sent':
                        $status = 4;
                        break;
                    case 'Failed':
                        $status = 3;
                        break;
                }
                $resultSMS = DB::connection('mysql')->table('report')->where('id', $m->message_id)
                    ->update([
                        'mobile_number' => $m->sms_send_to,
                        'sms_content' => $m->sms_message,
                        'sms_response_code' => $m->sms_respond_code,
                        'sms_status' => $status,
                        'updated_at' => $m->updated_at
                    ]);
                print_r("\n SMS Records: " . $smsidx++);
                print_r(" ==================> inserted: " . $resultSMS);
            }
        }
    }
}
