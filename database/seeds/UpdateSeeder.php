<?php

use Illuminate\Database\Seeder;

class UpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start_time = microtime(true);
        $password = bcrypt('Sentosa1');

        //code
        $codes = DB::connection('senims')->select('select * from code_counts');
        foreach ($codes as $c) {
            DB::connection('mysql')->table('code')->where('id', $c->id)
                ->update([
                'name' => $c->code,
                'total' => $c->max_count
            ]);

        }

        $this->call(UpdateUserSeeder::class);
        $this->call(UpdateIncidentSeeder::class);
        $this->call(UpdateBroadcastSeeder::class);
        $this->call(UpdateMessageSeeder::class);
        $this->call(UpdateEmailMessageSeeder::class);
        $this->call(UpdateSMSMessageSeeder::class);
        $this->call(UpdatePushMessageSeeder::class);
        $this->call(UpdateAuditSeeder::class);
        $end_time = microtime(true);
        print_r("Execution time => ".($end_time - $start_time). ' seconds');
    }
}
