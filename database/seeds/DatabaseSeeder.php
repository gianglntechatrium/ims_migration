<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start_time = microtime(true);
         $this->call(SecurityTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(DepartmentTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(IncidentTableSeeder::class);
         $this->call(BroadcastTableSeeder::class);
         $this->call(ReportTableSeeder::class);
         $this->call(EmailReportTableSeeder::class);
         $this->call(SMSReportTableSeeder::class);
         $this->call(PushReportTableSeeder::class);
         $this->call(AuditTableSeeder::class);
         $end_time = microtime(true);
         print_r("Execution time => ".($end_time - $start_time). ' seconds');
    }
}
