<?php

use Illuminate\Database\Seeder;

class UpdatePushMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // push_messages
        $pushidx = 0;
        $total = DB::connection('senims')->select("select count(id) total from push_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06')))")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $email_messages = DB::connection('senims')->select("select message_id, title, status, updated_at
                                                            from push_messages where message_id IN (select id from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06'))) order by id desc limit 1000 offset ".($i * 1000));
            foreach ($email_messages as $m) {
                $updated_at = "";
                if (!empty($m->updated_at)) {
                    $myTime = strtotime($m->updated_at);
                    $updated_at = date('Y-m-d H:i:s', $myTime);
                }

                $status = 1;
                switch ($m->status) {
                    case 'Delivered':
                        $status = 4;
                        break;
                    case 'Queued':
                        $status = 1;
                        break;
                    case 'Read':
                        $status = 4;
                        break;
                    case 'Failed':
                        $status = 3;
                        break;
                }

                $resultPush = DB::connection('mysql')->table('report')->where('id', $m->message_id)
                    ->update([
                        'push_content' => $m->title,
                        'push_notification_status' => $status,
                        'is_read' => $m->status == 'Read' ? 1 : 0,
                        'updated_at' => $m->updated_at
                    ]);
                print_r("\n Push Records: " . $pushidx++);
                print_r(" ==================> inserted: " . $resultPush);
            }
        }
    }
}
