<?php

use Illuminate\Database\Seeder;

class UpdateMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idx = 0;
        // messages
        $total = DB::connection('senims')->select("select count(id) total from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06'))")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $messages = DB::connection('senims')->select("select id, broadcast_id, profile_id, department, phone_number, email_address, send_email,
                                                            send_sms, status, responded_value, created_at, updated_at, respond_code, is_read,
                                                            is_responded
                                                      from messages where broadcast_id IN (select id from broadcasts where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06')) order by id desc limit 1000 offset ".($i * 1000));
            foreach ($messages as $m) {
                $message_existed = DB::connection('mysql')->select("select id from report where id=?", [$m->id]);
                if (empty($message_existed)) {
                    $department_id = "";
                    if (!empty($m->department)) {
                        $department_id = DB::connection('mysql')->select("SELECT id FROM department WHERE name='" . $m->department . "'");
                    }

                    $created_at = "";
                    if (!empty($m->created_at)) {
                        $myTime = strtotime($m->created_at);
                        $created_at = date('Y-m-d H:i:s', $myTime);
                    }

                    $updated_at = "";
                    if (!empty($m->updated_at)) {
                        $myTime = strtotime($m->updated_at);
                        $updated_at = date('Y-m-d H:i:s', $myTime);
                    }

                    $incident = "";
                    if (!empty($m->broadcast_id)) {
                        $incident = DB::connection('mysql')->select("select incident_id from broadcast");
                    }

                    $result = DB::connection('mysql')->table('report')->insert([
                        'id' => $m->id,
                        'broadcast_id' => $m->broadcast_id,
                        'incident_id' => !empty($incident) ? $incident[0]->incident_id : NULL,
                        'acknowledged_response' => !empty($m->responded_value) ? 1 : 0,
                        'enabled_push_notification' => 1,
                        'active' => 1,
                        'enabled_email' => $m->send_email,
                        'email' => $m->email_address,
                        'enabled_sms' => $m->send_sms,
                        'mobile_number' => $m->phone_number,
                        'sms_response_code' => $m->respond_code,
                        'is_responsed' => $m->is_responded,
                        'is_read' => $m->is_read,
                        'user_id' => $m->profile_id,
                        'department_id' => !empty($department_id) ? $department_id[0]->id : NULL,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ]);

                    print_r("\n Message Records: " . $idx++);
                    print_r(" ==================> inserted: " . $result);
                }
            }
        }
    }
}
