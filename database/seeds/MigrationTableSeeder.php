<?php
ini_set('memory_limit', '999999M');
use Illuminate\Database\Seeder;

class MigrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users not role
        $user_not_roles = DB::connection('mysql')->select("select id from users where id NOT IN (select user_id from role_user)");
        foreach ($user_not_roles as $u) {
            DB::connection('mysql')->table('role_user')->insert([
                'user_id' => $u->id,
                'role_id' => 1
            ]);
        }
    }
}