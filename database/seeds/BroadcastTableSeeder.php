<?php

use Illuminate\Database\Seeder;

class BroadcastTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Broadcasts
        $idx = 0;
        $total = DB::connection('senims')->select("select count(id) total from broadcasts
                                                        where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06')")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $broadcasts = DB::connection('senims')->select("select id, broadcast_status, broadcast_title, broadcast_description, respond_type,
                                                            respond_deadline, respond_code,enable_push, enable_email, email_subject, email_body,
                                                            enable_sms, sms_message, enable_filter, filter_departments, filter_categories, filter_groups, individual_profiles,
                                                            enabled, created_by, created_at, updated_at, related_id, related_type
                                                        from broadcasts
                                                        where related_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06') order by id desc limit 1000 offset ".($i * 1000));
            foreach ($broadcasts as $b) {
                $status = 2;
                switch ($b->broadcast_status) {
                    case 'draft':
                        $status = 0;
                        break;
                    case 'in_progress':
                        $status = 1;
                        break;
                    case 'completed':
                        $status = 2;
                        break;
                }

                $deadline = "";
                if (!empty($b->respond_deadline)) {
                    $myTime = strtotime($b->respond_deadline);
                    $deadline = date('Y-m-d H:i:s', $myTime);
                }

                $type = 1;
                switch ($b->related_type) {
                    case 'FIR':
                        $type = 0;
                        break;
                    case 'Additional':
                        $type = 1;
                        break;
                    case 'FinalReport':
                        $type = 2;
                        break;
                }

                $department = "";
                if (!empty($b->filter_departments)) {
                    $department_cris = DB::connection('mysql')->select('select id from department where FIND_IN_SET(name, ?) > 0', [$b->filter_departments]);
                    $department = implode(',', array_column($department_cris, 'id'));
                }

                $category = "";
                if (!empty($b->filter_categories)) {
                    $categories = DB::connection('mysql')->select('select id from category where FIND_IN_SET(name, ?) > 0', [$b->filter_categories]);
                    $category = implode(',', array_column($categories, 'id'));
                }

                $created_at = "";
                if (!empty($b->created_at)) {
                    $myTime = strtotime($b->created_at);
                    $created_at = date('Y-m-d H:i:s', $myTime);
                }

                $updated_at = "";
                if (!empty($b->updated_at)) {
                    $myTime = strtotime($b->updated_at);
                    $updated_at = date('Y-m-d H:i:s', $myTime);
                }

                $result = DB::connection('mysql')->table('broadcast')->insert([
                    'id' => $b->id,
                    'title' => $b->broadcast_title,
                    'status' => $status,
                    'description' => $b->broadcast_description,
                    'acknowledgement' => $b->respond_type == 'ack' ? 1 : 0,
                    'deadline' => !empty($deadline) ? $deadline : NULL,
                    'respond_code' => $b->respond_code,
                    'incident_id' => $b->related_id,
                    'is_push_notification' => $b->enable_push,
                    'is_first' => $type,
                    'push_content' => '',
                    'is_email_notification' => $b->enable_email,
                    'email_subject' => $b->email_subject,
                    'email_content' => $b->email_body,
                    'is_sms_notification' => $b->enable_sms,
                    'sms_content' => $b->sms_message,
                    'active' => $b->enabled,
                    'departments' => !empty($department) ? $department : '',
                    'categories' => !empty($category) ? $category : '',
                    'recipients' => $b->individual_profiles,
                    'enabled_filter' => $b->enable_filter,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ]);
                print_r("\n Broadcast Records: " . $idx++);
                print_r(" ==================> inserted: " . $result);
            }
        }
    }
}
