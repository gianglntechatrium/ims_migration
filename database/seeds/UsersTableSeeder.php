<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('Sentosa1');
        // Designation
        $designations = DB::connection('senims')->select('select * from designation');
        foreach ($designations as $d) {
            DB::connection('mysql')->table('designation')->insert([
                'name' => $d->designation_name,
                'active' => $d->enabled
            ]);
        }

        // Permissions
        $permissions = DB::connection('senims')->table('permissions')->get();

        foreach ($permissions as $permission) {
            DB::connection('mysql')->table('permissions')->insert([
                'id' => $permission->id,
                'name' => $permission->name,
                'display_name' => $permission->display_name,
                'description' => $permission->description,
                'created_at' => $permission->created_at,
                'updated_at' => $permission->updated_at
            ]);
        }

        // Roles
        $roles = DB::connection('senims')->table('roles')->get();

        foreach ($roles as $role) {
            DB::connection('mysql')->table('roles')->insert([
                'id' => $role->id,
                'name' => $role->name,
                'display_name' => $role->display_name,
                'description' => $role->description,
                'created_at' => $role->created_at,
                'updated_at' => $role->updated_at
            ]);
        }

        // Permission Roles
        $permission_roles = DB::connection('senims')->table('permission_role')->get();

        foreach ($permission_roles as $permission_role) {
            DB::connection('mysql')->table('permission_role')->insert([
                'permission_id' => $permission_role->permission_id,
                'role_id' => $permission_role->role_id
            ]);
        }

        // Users
        $users = DB::connection('senims')->select("select p.id, u.name, u.email, u.password, p.sur_name, p.mobile_number,
                                                  p.department, p.enabled, p.is_deleted, p.designation, p.email_criteria_list,
                                                  p.sms_criteria_list, p.push_criteria_list, p.receive_mail, p.receive_sms,
                                                  p.receive_push, p.department_criteria_list, p.created_at, p.updated_at, p.date_join
        from profiles p
		left join users u on p.user_id = u.id");

        foreach ($users as $user) {
            //TODO: department not exist (CO,SSGT,HOT,OOS,Duty Officer,MTI - Industry Division, No Department)
            print_r("\nUser => ".$user->department);
            $department = $user->department == 'CO' || $user->department == 'SSGT' ? 'SPF' : $user->department;
            $department_cri = $user->department_criteria_list == 'CO' || $user->department_criteria_list == 'SSGT' ? 'SPF' : $user->department_criteria_list;
            $department_id = DB::connection('mysql')->select("SELECT id FROM department WHERE name='".$department."'");
            $department_cris = DB::connection('mysql')->select('select id from department where FIND_IN_SET(name, ?) > 0', [$department_cri]);
            $department_cri_ids = implode(',', array_column($department_cris, 'id'));
            DB::connection('mysql')->table('users')->insert([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'password' => $password,
                'fullname' => $user->sur_name,
                'mobile_number' => $user->mobile_number,
                'active' => $user->enabled,
                'designation' => $user->designation,
                'department_id' => !empty($department_id) ? $department_id[0]->id : NULL,
                'receive_sms' => $user->receive_sms,
                'sms_criteria_list' => $user->sms_criteria_list,
                'receive_email' => $user->receive_mail,
                'email_criteria_list' => $user->email_criteria_list,
                'receive_push_notification' => $user->receive_push,
                'push_criteria_list' => $user->push_criteria_list,
                'department_criteria' => $department_cri_ids,
                'is_deleted' => $user->is_deleted,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'last_login' => $user->date_join == '0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $user->date_join
            ]);
        }

        // Role_Users
        $user_roles = DB::connection('senims')->select("select p.id, r.role_id
                                                        from role_user r
                                                        left join profiles p on p.user_id = r.user_id where p.id is not NULL");

        foreach ($user_roles as $r) {
            DB::connection('mysql')->table('role_user')->insert([
                'user_id' => $r->id,
                'role_id' => $r->role_id
            ]);
        }
        $departmentAdmin = DB::connection('mysql')->select("SELECT id FROM department WHERE name='Project Development'");
        // create administrator
        DB::connection('mysql')->table('users')->insert([
            'name' => 'Administrator',
            'email' => 'sysadmin@techatrium.com',
            'password' => $password,
            'fullname' => 'Administrator',
            'active' => 1,
            'designation' => 'Administrator',
            'department_id' => $departmentAdmin[0]->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $admin = DB::connection('mysql')->select("select id from users where name = 'Administrator'");
        $roleAdmin = DB::connection('mysql')->select("select id from roles where name = 'Administrator'");

        DB::connection('mysql')->table('role_user')->insert([
            'user_id' => $admin[0]->id,
            'role_id' => $roleAdmin[0]->id
        ]);
    }
}
