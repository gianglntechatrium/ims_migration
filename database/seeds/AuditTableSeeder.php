<?php

use Illuminate\Database\Seeder;

class AuditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Audit
        $idx = 0;
        $total = DB::connection('senims')->select("select count(id) total from audit where user_id IN (select user_id from profiles)")[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $audits = DB::connection('senims')->select("select * from audit where user_id IN (select user_id from profiles)  order by id desc limit 1000 offset ".($i * 1000));
            foreach ($audits as $a) {
                $created_at = "";
                if (!empty($a->created_at)) {
                    $myTime = strtotime($a->created_at);
                    $created_at = date('Y-m-d H:i:s', $myTime);
                }

                $updated_at = "";
                if (!empty($a->updated_at)) {
                    $myTime = strtotime($a->updated_at);
                    $updated_at = date('Y-m-d H:i:s', $myTime);
                }

                $user_id = DB::connection('senims')->select("select id from profiles where user_id=?", [$a->user_id]);
                $module = "";
                switch ($a->module_name) {
                    case 'Profiles':
                        $module = 'User';
                        break;
                    case 'Incidents':
                        $module = 'Incident';
                        break;
                }

                $result = DB::connection('mysql')->table('audit')->insert([
                    'id' => $a->id,
                    'user_id' => $user_id[0]->id,
                    'object_id' => $a->object_id,
                    'module_name' => $module,
                    'action' => $a->action,
                    'activity' => $a->activity,
                    'before' => $a->before,
                    'after' => $a->after,
                    'difference' => $a->difference,
                    'audit_migrate' => $a->audit_migrate,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ]);
                print_r("\n Audit Records: " . $idx++);
                print_r(" ==================> inserted: " . $result);
            }
        }
    }
}
