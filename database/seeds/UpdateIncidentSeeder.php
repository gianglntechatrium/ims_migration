<?php

use Illuminate\Database\Seeder;

class UpdateIncidentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // incident
        $idx = 0;
        $incidents_existed = DB::connection('mysql')->select("select id from incident");
        $total = DB::connection('senims')->select("select count(id) total from incidents where created_at >= '2017-04-20 16:14:06' AND FIND_IN_SET(id, ?) < 1", [implode(",", array_column($incidents_existed, 'id'))])[0]->total;
        $records = ($total / 1000)+1;
        for ($j = 0; $j < intval($records); $j++) {
            $incidents = DB::connection('senims')->select("select id, incident_code, start_date, category, department, department_info,
                                                        location_description, reported_by, attended_by, attachments, level1, level2, level3,
                                                        location_checkpoint, noo_nto, mtsg_issued, police_report_no, title, description,
                                                        incident_status_id, action_taken, case_summary, is_private, latitude, longtitude,
                                                        owner_email, is_deleted, created_at, updated_at, follow_up_action,
                                                        incident_status_id
                                                       from incidents where created_at >= '2017-04-20 16:14:06' AND FIND_IN_SET(id, ?) < 1 order by id desc limit 1000 offset " . ($j * 1000), [implode(",", array_column($incidents_existed, 'id'))]);
            foreach ($incidents as $i) {
                $category = '';
                if (!empty($i->category)) {
                    $category = DB::connection('mysql')->select("select id from category where name=?", [$i->category]);
                }

                $department = "";
                if (!empty($i->department)) {
                    $department_cris = DB::connection('mysql')->select('select id from department where FIND_IN_SET(name, ?) > 0', [$i->department]);
                    $department = implode(',', array_column($department_cris, 'id'));
                }

                $department_info = "";
                if (!empty($i->department_info)) {
                    $department_cris = DB::connection('mysql')->select('select id from department where FIND_IN_SET(name, ?) > 0', [$i->department_info]);
                    $department_info = implode(',', array_column($department_cris, 'id'));
                }

                $level1 = "";
                if (!empty($i->level1)) {
                    $level1 = DB::connection('mysql')->select("select id from security where name=?", [$i->level1]);
                }

                $level2 = "";
                if (!empty($i->level2)) {
                    $level2 = DB::connection('mysql')->select("select id from security where name=?", [$i->level2]);
                }

                $level3 = "";
                if (!empty($i->level3)) {
                    $level3 = DB::connection('mysql')->select("select id from security where name=?", [$i->level3]);
                }

                $location = "";
                if (!empty($i->location_checkpoint)) {
                    $location = DB::connection('mysql')->select("select id, latitude, longitude from location where name=?", [$i->location_checkpoint]);
                }

                $status = '';
                switch ($i->incident_status_id) {
                    case 10:
                        $status = 0;
                        break;
                    case 11:
                        $status = 2;
                        break;
                    case 12:
                        $status = 3;
                        break;
                    default:
                        $status = $i->incident_status_id;
                }

                $user = '';
                if (!empty($i->owner_email)) {
                    $user = DB::connection('mysql')->select("select id from users where email=?", [$i->owner_email]);
                }

                print_r("\n");
                print_r($i->start_date);
                print_r("\n");

                $result = DB::connection('mysql')->table('incident')->insert([
                    'id' => $i->id,
                    'code' => $i->incident_code,
                    'start_time' => !empty($i->start_date) ? $i->start_date : NULL,
                    'category_id' => !empty($category) ? $category[0]->id : NULL,
                    'department' => !empty($department) ? $department : '',
                    'department_info' => !empty($department_info) ? $department_info : '',
                    'follow_up_action' => $i->follow_up_action,
                    'security_level_1' => !empty($level1) ? $level1[0]->id : NULL,
                    'security_level_2' => !empty($level2) ? $level2[0]->id : NULL,
                    'security_level_3' => !empty($level3) ? $level3[0]->id : NULL,
                    'location_id' => !empty($location) ? $location[0]->id : NULL,
                    'location_description' => $i->location_description,
                    'report_by' => $i->reported_by,
                    'attend_by' => $i->attended_by,
                    'attachments' => $i->attachments,
                    'noo_nto' => $i->noo_nto,
                    'mtsg_no' => $i->mtsg_issued,
                    'is_public' => $i->is_private == 1 ? 0 : 1,
                    'is_deleted' => $i->is_deleted == 1 ? 1 : 0,
                    'police_report_no' => $i->police_report_no,
                    'status' => $status,
                    'title' => $i->title,
                    'description' => $i->description,
                    'action_taken' => $i->action_taken,
                    'case_summary' => $i->case_summary,
                    'user_id' => !empty($user) ? $user[0]->id : NULL,
                    'created_at' => $i->created_at,
                    'updated_at' => $i->updated_at,
                    'latitude' => !is_null($i->latitude) ? $i->latitude : $location[0]->latitude,
                    'longitude' => !is_null($i->longtitude) ? $i->longtitude : $location[0]->longitude
                ]);
                print_r("\nIncidents Records: " . $idx++);
                print_r(" " . $i->incident_code . " ");
                print_r("inserted: " . $result);
            }
        }

        // People
        $idxPeople = 0;
        $total = DB::connection('senims')->select("select count(id) total from incident_particulars where incident_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06' AND FIND_IN_SET(id, ?) < 1)", [implode(",", array_column($incidents_existed, 'id'))])[0]->total;
        $records = ($total / 1000)+1;
        for ($i = 0; $i < intval($records); $i++) {
            $peoples = DB::connection('senims')->select("select * from incident_particulars where incident_id IN (select id from incidents where created_at >= '2017-04-20 16:14:06' AND FIND_IN_SET(id, ?) < 1) order by id desc limit 1000 offset " . ($i * 1000), [implode(",", array_column($incidents_existed, 'id'))]);
            foreach ($peoples as $p) {
                $result = DB::connection('mysql')->table('people')->insert([
                    'id' => $p->id,
                    'name' => $p->name,
                    'gender' => $p->gender,
                    'address' => $p->address,
                    'date_of_birth' => $p->date_of_birth,
                    'age' => $p->age,
                    'contact' => $p->contact_number,
                    'nationality' => $p->nationality,
                    'email' => $p->email,
                    'nric_passport' => $p->passport_no,
                    'incident_id' => $p->incident_id
                ]);
                print_r("\n People Records: " . $idx++);
                print_r(" ==================> inserted: " . $result);
            }
        }
    }
}
