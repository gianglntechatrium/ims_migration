<?php

use Illuminate\Database\Seeder;

class UpdateCriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::connection('senims')->select("select p.id, u.name, u.email, u.password, p.sur_name, p.mobile_number,
                                                  p.department, p.enabled, p.is_deleted, p.designation, p.email_criteria_list,
                                                  p.sms_criteria_list, p.push_criteria_list, p.receive_mail, p.receive_sms,
                                                  p.receive_push, p.department_criteria_list, p.created_at, p.updated_at, p.date_join, p.status
        from profiles p
		left join users u on p.user_id = u.id");

        foreach ($users as $user) {
            $sms_list = DB::connection('mysql')->select("select id from category where FIND_IN_SET(name, ?)", [$user->sms_criteria_list]);
            $email_list = DB::connection('mysql')->select("select id from category where FIND_IN_SET(name, ?)", [$user->email_criteria_list]);
            $push_list = DB::connection('mysql')->select("select id from category where FIND_IN_SET(name, ?)", [$user->push_criteria_list]);
            DB::connection('mysql')->table('users')->where('id', $user->id)
                                    ->update([
                                        'active' => $user->enabled,
                                        'is_deleted' => $user->status == 'Active' ? 0 : 1,
                                        'receive_sms' => $user->receive_sms,
                                        'receive_email' => $user->receive_mail,
                                        'receive_push_notification' => $user->receive_push,
                                        'sms_criteria_list' => implode(",", array_column($sms_list, 'id')),
                                        'email_criteria_list' => implode(",", array_column($email_list, 'id')),
                                        'push_criteria_list' => implode(",", array_column($push_list, 'id'))
                                    ]);
        }
    }
}
