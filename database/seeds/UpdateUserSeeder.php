<?php

use Illuminate\Database\Seeder;

class UpdateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Update users Table
        $users_existed = DB::connection('mysql')->select("select id from users");
        $users = DB::connection('senims')->select("select p.id, u.name, u.email, u.password, p.sur_name, p.mobile_number,
                                                  p.department, p.enabled, p.is_deleted, p.designation, p.email_criteria_list,
                                                  p.sms_criteria_list, p.push_criteria_list, p.receive_mail, p.receive_sms,
                                                  p.receive_push, p.department_criteria_list, p.created_at, p.updated_at, p.date_join
        from profiles p
		left join users u on p.user_id = u.id where FIND_IN_SET(p.id, ?) < 1", [implode(",", array_column($users_existed, 'id'))]);

        print_r($users);

        foreach ($users as $user) {
            //TODO: department not exist (CO,SSGT,HOT,OOS,Duty Officer,MTI - Industry Division, No Department)
            print_r("\nUser => ".$user->department);
            $department = $user->department == 'CO' || $user->department == 'SSGT' ? 'SPF' : $user->department;
            $department_cri = $user->department_criteria_list == 'CO' || $user->department_criteria_list == 'SSGT' ? 'SPF' : $user->department_criteria_list;
            $department_id = DB::connection('mysql')->select("SELECT id FROM department WHERE name='".$department."'");
            $department_cris = DB::connection('mysql')->select('select id from department where FIND_IN_SET(name, ?) > 0', [$department_cri]);
            $department_cri_ids = implode(',', array_column($department_cris, 'id'));
            DB::connection('mysql')->table('users')->insert([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'password' => $password,
                'fullname' => $user->sur_name,
                'mobile_number' => $user->mobile_number,
                'active' => $user->enabled,
                'designation' => $user->designation,
                'department_id' => !empty($department_id) ? $department_id[0]->id : NULL,
                'receive_sms' => $user->receive_sms,
                'sms_criteria_list' => $user->sms_criteria_list,
                'receive_email' => $user->receive_mail,
                'email_criteria_list' => $user->email_criteria_list,
                'receive_push_notification' => $user->receive_push,
                'push_criteria_list' => $user->push_criteria_list,
                'department_criteria' => $department_cri_ids,
                'is_deleted' => $user->is_deleted,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'last_login' => $user->date_join == '0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $user->date_join
            ]);
        }

        // Role_Users
        $user_roles = DB::connection('senims')->select("select p.id, r.role_id
                                                        from role_user r
                                                        left join profiles p on p.user_id = r.user_id where p.id is not NULL AND FIND_IN_SET(p.id, ?) < 1", [implode(",", array_column($users_existed, 'id'))]);
        print_r($user_roles);
        foreach ($user_roles as $r) {
            DB::connection('mysql')->table('role_user')->insert([
                'user_id' => $r->id,
                'role_id' => $r->role_id
            ]);
        }
    }
}
