<?php

use Illuminate\Database\Seeder;

class SecurityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Code
        $codes = DB::connection('senims')->select('select * from code_counts');
        foreach ($codes as $c) {
            DB::connection('mysql')->table('code')->insert([
                'id' => $c->id,
                'name' => $c->code,
                'total' => $c->max_count
            ]);
        }

        // Security
        // Level 1
        $level1 = DB::connection('senims')->select("select id, type_name, enabled
                                                      from type_incident_level where parent = ''");
        foreach ($level1 as $l) {
            DB::connection('mysql')->table('security')->insert([
                'id' => $l->id,
                'name' => $l->type_name,
                'active' => $l->enabled
            ]);
        }
        // Level 2
        $level2 = DB::connection('senims')->select("select id, type_name, enabled, parent
                                                      from type_incident_level where level2 != '' AND level3 = ''");
        foreach ($level2 as $l) {
            $parent_id = DB::connection('mysql')->select("select id from security where name='".$l->parent."'");
            DB::connection('mysql')->table('security')->insert([
                'id' => $l->id,
                'name' => $l->type_name,
                'parent_id' => $parent_id[0]->id,
                'active' => $l->enabled
            ]);
        }
        // Level 3
        $level3 = DB::connection('senims')->select("select id, type_name, enabled, parent
                                                      from type_incident_level where level3 != ''");
        foreach ($level3 as $l) {
            $parent_id = DB::connection('mysql')->select("select id from security where name='".$l->parent."'");
            DB::connection('mysql')->table('security')->insert([
                'id' => $l->id,
                'name' => $l->type_name,
                'parent_id' => $parent_id[0]->id,
                'active' => $l->enabled
            ]);
        }

        // Location
        $locations = DB::connection('ims')->select('select * from location');
        foreach ($locations as $l) {
            $enabled = DB::connection('senims')->select("select enabled from location_incident where location_name='".$l->name."'");
            DB::connection('mysql')->table('location')->insert([
                'id' => $l->id,
                'code' => $l->code,
                'name' => $l->name,
                'active' => $enabled[0]->enabled,
                'longitude' => $l->longitude,
                'latitude' => $l->latitude
            ]);
        }

        // header
        $header = DB::connection('ims')->select('select * from header');
        foreach ($header as $h) {
            DB::connection('mysql')->table('header')->insert([
                'id' => $h->id,
                'table_structure' => $h->table_structure,
                'json_key' => $h->json_key,
                'table_id' => $h->table_id
            ]);
        }
    }
}
