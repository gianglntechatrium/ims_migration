<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = DB::connection('mysql')->select("select id from category");
        $categories_id = implode(",", array_column($categories, 'id'));

        // Departments
        $departments = DB::connection('senims')->table('departments')->get();
        foreach ($departments as $department) {
            DB::connection('mysql')->table('department')->insert([
                'name' => $department->dept_name,
                'active' => $department->enabled,
                'sms_criteria' => $categories_id,
                'email_criteria' => $categories_id,
                'push_criteria' => $categories_id
            ]);
        }
        DB::connection('mysql')->table('department')->insert([
            'name' => 'HOT',
            'active' => $department->enabled,
            'sms_criteria' => $categories_id,
            'email_criteria' => $categories_id,
            'push_criteria' => $categories_id
        ]);
        DB::connection('mysql')->table('department')->insert([
            'name' => 'OOS',
            'active' => $department->enabled,
            'sms_criteria' => $categories_id,
            'email_criteria' => $categories_id,
            'push_criteria' => $categories_id
        ]);
        DB::connection('mysql')->table('department')->insert([
            'name' => 'Duty Officer',
            'active' => $department->enabled,
            'sms_criteria' => $categories_id,
            'email_criteria' => $categories_id,
            'push_criteria' => $categories_id
        ]);
        DB::connection('mysql')->table('department')->insert([
            'name' => 'MTI - Industry Division',
            'active' => $department->enabled,
            'sms_criteria' => $categories_id,
            'email_criteria' => $categories_id,
            'push_criteria' => $categories_id
        ]);
        DB::connection('mysql')->table('department')->insert([
            'name' => 'SPF',
            'active' => $department->enabled,
            'sms_criteria' => $categories_id,
            'email_criteria' => $categories_id,
            'push_criteria' => $categories_id
        ]);
    }
}
