<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Category
        $categories = DB::connection('senims')->select("select * from incident_categories");
        foreach ($categories as $c) {
            DB::connection('mysql')->table('category')->insert([
                'code' => $c->category_shortname,
                'name' => $c->category_name,
                'active' => $c->enabled
            ]);
        }
    }
}
